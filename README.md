# Laboratorio 3 TPP: La papa caliente

## Que es la papa caliente ?

Un grupo de amigos, sentados alrededor de una fogata, juegan a pasarse una papa 
caliente recién sacada de entre las brasas. Como la papa está tan caliente, el 
primero que la saca inmediatamente se la pasa al compañero de su derecha, quien 
a su vez se la pasa a su compañero de su derecha. Así, la papa circula 
alrededor de la fogata. Cuando uno de ellos no es capaz de sostener la papa ni 
siquiera por un instante dado, la bota al suelo, y pierde. Entonces, el 
compañero de la derecha saca una nueva papa y continua el juego, pero ahora el 
que perdió no participa.

El ganador es el último que permanece sin que se le haya caído la papa.

## Compilación

En la carpeta `lab1/` ejecute `make`.

## Ejecución

En la carpeta `lab1/` ejecute:

	$ mpirun -n PLAYERS ./hotpotato VALOR_INICIAL

Donde `PLAYERS` es la cantidad de jugadores, y `VALOR_INICIAL` es el valor con 
el que empieza el token.

## Bugs

Los mensajes se imprimen desordenados.


## Por implementar

 - [ ] Los procesos en vez de imprimir por la salida estándar, le pasen el 
   mensaje al proceso 0
 - [ ] Evitar que los mensajes se impriman en orden incorrecto
