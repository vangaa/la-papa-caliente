CC = @mpicxx
CFLAGS = -Wall -ansi

PROGNAME = hotpotato
OBJ = Player.o main.o
OBJDIR = build/obj
VPATH = src/

.PHONY: all clean

all: $(PROGNAME)

$(PROGNAME): $(addprefix $(OBJDIR)/, $(OBJ))
	@echo Creando ejecutable «$@»...
	$(CC) -o $@ $^ $(CFLAGS)

$(OBJDIR):
	@echo Creando directorio de salida...
	@mkdir -p $@

$(OBJDIR)/%.o: %.cc | $(OBJDIR)
	@echo Compilando «$<»...
	$(CC) -o $@ -c $< $(CFLAGS)

# dependencias

$(OBJDIR)/Player.o: Player.hh

clean:
	@echo Limpiando...
	@rm -f $(PROGNAME)
	@rm -rf $(addprefix $(OBJDIR)/, $(OBJ))
