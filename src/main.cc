#include "Player.hh"
#include <iostream>
#include <mpi.h>

#ifndef MPI_
#define MPI_ MPI::COMM_WORLD
#endif

using namespace std;

int main(int argc, char* argv[])
{
	MPI::Init(argc, argv);

	int myrank = MPI_.Get_rank();

	if (myrank == 0 && argc < 2)
	{
		cerr << "Falta un argumento\n";
		MPI_.Abort(MPI::ERR_OTHER);
	}

	stringstream s;
	int valor_papa;

	s << argv[1];
	s >> valor_papa;

	Player jugador;

	jugador.play(valor_papa);

	MPI::Finalize(); 
}
