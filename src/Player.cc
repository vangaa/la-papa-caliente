#include <iostream>
#include "Player.hh"

using namespace std;

Player::Player()
{
	myrank = MPI_.Get_rank();
	nproc = MPI_.Get_size();

	ingame = new bool[nproc];

	for (int i = 0; i< nproc; i++)
	{
		ingame[i] = true;
	}

	leftp = (myrank-1+nproc) % nproc;
	rightp = (myrank + 1) % nproc;

	srand(time(NULL)); 
}

void Player::pass_potato()
{
	potato -= rand() % GAP_MAX;
	MPI_.Send(&potato, 1, MPI::INT, rightp, POTATO);
}

void Player::receive_potato()
{
	MPI_.Recv(&potato, 1, MPI::INT, leftp, POTATO);

	cout << "Proceso "<< myrank << " tiene la papa con valor "<< potato;

	if (potato < 0)
	{
		cout << " (proceso "<< myrank << " sale del juego)\n";

		kick_player(myrank);
	}
	else
	{
		cout << endl;
		pass_potato();
	}
}

void Player::kick_player(int rank)
{
	bool reiniciar = false;	//!< indica si tengo que reiniciar el juego
	bool ultimo = last_in_game();	//!< indica si soy el ultimo

	ingame[rank] = false;

	if (rank == myrank)
	{ // soy yo al que estamos eliminando
		for (int proc = 0; proc < nproc; proc++)
		{ // mando mi rank a todos los procesos para que me eliminen del juego
			if (proc != myrank)
			{
				MPI_.Send(&myrank, 1, MPI::INT, proc, LOSER);
			}
		}

		if (!ultimo)
		{ // si no soy el ultimo
			MPI_.Send(&leftp, 1, MPI::INT, rightp, RANK);
			MPI_.Send(&rightp, 1, MPI::INT, leftp, RANK);
		}
		else ingame[myrank] = true; // solo yo sé que estoy jugando

		leftp = rightp = -1;	// me quedo sin jugadores adyacentes
	}
	if (rank == leftp)
	{ // estamos pateando al jugador de mi izquierda

		if (ingame[myrank]) reiniciar = true;

		MPI_.Recv(&leftp, 1, MPI::INT, leftp, RANK);
	}
	if (rank == rightp)
	{ // estamos pateando al jugador de mi derecha
		MPI_.Recv(&rightp, 1, MPI::INT, rightp, RANK);
	}

	MPI_.Barrier();


	// mando una nueva papa al jugador de la derecha
	if (reiniciar && rightp != myrank)
	{ // si debo reiniciar y no soy el ultimo
		potato = pot_ini;
		cout << "Proceso "<< myrank << " tiene la papa con valor "<< potato<< endl;
		pass_potato();
	}
	else if (rightp == myrank)
	{
		kick_player(myrank);
	}
}

void Player::debug_msg(string msg)
{
	stringstream s;

	s << "\e[0;31m["<< myrank << ": "<< msg << "]\e[0m";

	cerr << s.str() << endl;
}

void Player::play(int valor_inicial)
{
	MPI::Status estado;
	int tag, loser, emisor;
	stringstream s;

	pot_ini = potato = valor_inicial;

	if (myrank == 0)
	{
		cout << "Proceso 0 tiene la papa con valor " << pot_ini << endl;
		pass_potato();
	}

	MPI_.Barrier();
	while (!is_over() && !last_in_game())
	{ // si todavía estoy jugando

		MPI_.Probe(MPI::ANY_SOURCE, MPI::ANY_TAG, estado);

		tag = estado.Get_tag();
		emisor = estado.Get_source();

		switch (tag)
		{
			case POTATO:
				receive_potato();
				break;

			case LOSER:
				MPI_.Recv(&loser, 1, MPI::INT, emisor, tag);
				kick_player(loser);

				break;

			default:
				MPI_.Abort(MPI::ERR_OTHER);
				break;
		}
	}

	if (last_in_game())
	{ // si soy el ultimo que quedó en el juego
		cout << "Proceso "<< myrank<< " es el ganador\n";
	}
}

void Player::check_victory()
{
	if (last_in_game())
	{ // si soy el último en el juego

		// salgo del juego
		ingame[myrank] = false;

		cout << "Proceso " << myrank << " es el ganador\n";
	}
}

bool Player::is_over()
{
	for (int proc = 0; proc< nproc; proc++)
	{
		if (ingame[proc]) return false;
	}

	return true;
}

bool Player::last_in_game()
{
	for (int i=0; i< nproc; i++)
	{
		if (i != myrank && ingame[i])
		{
			return false;
		}
	}

	return ingame[myrank];
}
