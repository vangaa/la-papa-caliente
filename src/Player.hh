#ifndef _H_PLAYER_
#define _H_PLAYER_

#include <mpi.h>
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>

//! máximo descuento que se le puede hacer al token
#define GAP_MAX 10

#ifndef MPI_
	// como siempre se utiliza el comunicador universal, se define esta macro 
	// para abstraernos del comunicador
	#define MPI_ MPI::COMM_WORLD
#endif

enum msg_type
{
	POTATO, //!< POTATO o token. Es el mensaje que se van pasando los jugadores en el anillo
	LOSER,	//!< mensaje que indica el rank del jugador que perdió
	RANK	//!< mensaje que lleva el rank de algún proceso
};

class Player
{
	private:
		bool *ingame;	//!< indica que jugadores todavía están jugando

		int myrank;
		int nproc;		//!< numero de jugadores
		int leftp;		//!< rank del jugador izquierdo
		int rightp;		//!< rank del jugador derecho

		int pot_ini;	//!< valor del token inicialmente
		int potato;		//!< el valor del último token recibido

	public:
		/**
		 * Constructor. Inicializa el jugador con sus compañeros adyacentes 
		 * correspondientes. Por ejemplo, si el proceso actual es el 3, 
		 * entonces el proceso adyacente izquierdo es el 2 y el derecho es el 
		 * 4, en caso de haber mas procesos, o el 0, en caso de existir solo 4 
		 * procesos.
		 */
		Player();

		/**
		 * Le pasa el token al proceso de la derecha. Antes de pasarlo le resta 
		 * un valor random al token.
		 */
		void pass_potato();

		/**
		 * Recibe el token que le acaban de enviar. Luego de recibir el token, 
		 * comprueba si es menor a 0. Si lo es, envía su rank a todos los demás 
		 * procesos (O(n)); si no lo es, le pasa el token al siguiente proceso.
		 */
		void receive_potato();

		/**
		 * Elimina a un jugador del juego.
		 * @param rank Es el rank del jugador que pierde
		 */
		void kick_player(int rank);
	
		/**
		 * Imprime un mensaje de debug.
		 * @param msg Mensaje que se imprime
		 */
		void debug_msg(std::string msg);

		/**
		 * Los jugadores empiezan a pasarse el token y a interaccionar entre 
		 * ellos.
		 */
		void play(int valor_inicial);

		/**
		 * El jugador revisa si ha ganado. Si lo ha hecho, entonces imprime un 
		 * mensaje notificando su victoria y luego sale del juego.
		 */
		void check_victory();

		/**
		 * El jugador comprueba si él es el último jugador que queda.
		 * @return true si los demás jugadores dejaron de jugar, y false en 
		 * otro caso.
		 */
		bool last_in_game();
		/**
		 * Dice si el juego se acabó, debido a que no hay jugadores en juego.
		 * @return true si no hay mas jugadores, y false si queda por lo menos 
		 * 1
		 */
		bool is_over();
};




#endif
